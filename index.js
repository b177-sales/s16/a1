console.log("Hello World");

let number = Number(prompt("Give me a number"));
console.log("The number you provided is " + number + '.')

for(number = number; number >= 0; number--){
	if(number % 10 === 0) {
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	}
	if (number % 5 === 0){
		console.log(number);
		continue;
	}
	if (number <= 50) {
		console.log("The current value is at 50. Terminating the loop.");
		break;
	}
}


let string = 'supercalifragilisticexpialidocious';
console.log(string)

let consonant = '';

for (let i = 0; i < string.length; i++){
	if (
		string[i] == "a" ||
		string[i] == "e" || 
		string[i] == "i" || 
		string[i] == "o" || 
		string[i] == "u"
		){
		continue;
	}
	else {
		consonant = consonant + string[i];
	}
}
console.log(consonant)